# Node-RED

This project is to help deploy Node-RED as an IDE to create flows as git projects and then be able to package them as docker containers with or _without_ the IDE (a.k.a. the UI flow editor).

## Requirements

You must have docker running and idealy docker-compose

## Setup

Before you start, you have to copy the .env.template to .env

```bash
cp .env.template .env
```

Then edit the .env file and change the `NODE_RED_CREDENTIAL_SECRET` to a random string, it will be use by node-red to encrypt the secrets that save into your flows, it will make them "safish" to commit them into plublic repos as long as your `NODE_RED_CREDENTIAL_SECRET` is not leaked.

## Start the IDE

```bash
docker-compose up -d node-red
```

There is a nice [guide](https://nodered.org/docs/user-guide/projects/#creating-your-first-project) on how to setup Node-RED to create your first project on the nodered web site. Go an create a project using the web [IDE](http://localhost:1880)

NOTE: It is _easier_ to use `https` instead of `ssh` when configuring your git client into Node-RED.

## Package your flows into a container

Each project that you create with Node-RED can be package as a Docker image. If you use as a base the gitlab repository [node-red-apps/template](https://gitlab.com/node-red-apps/template.git) you will have all the necessary files to package your project as such.

The important files from that repo, if you want to copy them manually into your projects are:

- `Dockerfile`
- `global-contex.js`
- `settings.js`
- `settings.template.js`

Optionnaly you can also include `.gitab-ci.yml` if your project is hosted on gitlab to build it automagically!

Once your project has these files, you can simply build and push the docker image to have a running container of your project.

```bash
docker build -t my-docker-handle/my-node-red-microservice-name .
docker push my-docker-handle/my-node-red-microservice-name
docker run --name my-node-red-microservice-name -tidp 1880:1880 my-docker-handle/my-node-red-microservice-name
```

NOTE:

> It is possible that Node-RED did not add all your Node-RED `nodes` dependencies into the `package.json` inside the project. If this is the case, you can copy them manually from this project (`./data/package.json`) `depencendies` section into your project `package.json` repo and **rebuild** your docker image.

### Environment varialbes

| Environment variable | Value                                                                                                                                                                            | Description                                                      |
| -------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------- |
| NODE_RED_ADMIN_ROOT  | false                                                                                                                                                                            | Disable the IDE (flow editor)                                    |
| HTTP_NODE_CORS       | {"origin":"\*","methods":"GET,HEAD,PUT,PATCH,POST,DELETE","allowedHeaders":"Content-Type,Authorization","preflightContinue":false,"credentials":true,"optionsSuccessStatus":204} | To allow CORS from all origins                                   |
| API_BASIC_AUTH_ROLES | {user:"admin", pass:"$2b$08$DRQGYVfJJrLBXtWLt03VEuORq6Ao/KQMfmdPDBh39RW9PA1TeE1My/7QfM4uru"}                                                                                     | Use Basic Auth on HTTP IN with user `admin` and password `admin` |

### To use github oAuth with the IDE

Make sure that the dependencies includes `node-red-auth-github` in the package.json file to be able to use github oAuth.

| Environment variable  | Value                                                     | Description                                   |
| --------------------- | --------------------------------------------------------- | --------------------------------------------- |
| GITHUB_CLIENT_ID      | XXXXXXXXXXX                                               | To use GITHUB oAuth for the IDE (flow editor) |
| GITHUB_CLIENT_SECRET  | XXXXXXXXXXX                                               | To use GITHUB oAuth for the IDE (flow editor) |
| GITHUB_CLIENT_BASEURL | "http://localhost:1880/"                                  | To use GITHUB oAuth for the IDE (flow editor) |
| GITHUB_CLIENT_ROLES   | [{"username":"YOUR-GITHUB-USERNAME","permissions":["*"]}] | To use GITHUB oAuth for the IDE (flow editor) |

To change the URLs served by Node-RED and the IDE

| Environment variable | Value | Description                                                  |
| -------------------- | ----- | ------------------------------------------------------------ |
| NODE_RED_API_ROOT    | api   | All the url from the HTTP IN node will be prefixed with /api |
| NODE_RED_ADMIN_ROOT  | ide   | The IDE (flow editor) will be under /ide                     |

```

```

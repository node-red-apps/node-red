
node-red:
	docker-compose up -d node-red
	open http://localhost:1880

danger:
	tar --exclude='node_modules' --exclude='.npm' --exclude='**/@eaDir' -cvjf .backup-`date +%Y%m%d%H%M%S`.tar.bz2 data
	echo rm -rf data/.npm data/lib data/node_modules data/projects data/.config* data/package* data/.sessions.json
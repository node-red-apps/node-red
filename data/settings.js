const settings = require("./settings.template");

// --- Global functions / modules made available to all nodes
settings.functionGlobalContext = require("./global-context");

// --- Disable the admin API and the Flow Editor
if (process.env.NODE_RED_ADMIN_ROOT !== undefined) {
  settings.httpAdminRoot =
    process.env.NODE_RED_ADMIN_ROOT === "false"
      ? false
      : process.env.NODE_RED_ADMIN_ROOT;
}
if (process.env.NODE_RED_API_ROOT !== undefined) {
  settings.httpNodeRoot = process.env.NODE_RED_API_ROOT;
}

// --- Enable CORS
if (process.env.HTTP_NODE_CORS !== undefined) {
  settings.httpNodeCors = JSON.parse(process.env.HTTP_NODE_CORS);
}

// Protect the Flow Editor with github oAuth
if (
  process.env.GITHUB_CLIENT_ID &&
  process.env.GITHUB_CLIENT_SECRET &&
  process.env.GITHUB_CLIENT_ROLES
) {
  settings.adminAuth = require("node-red-auth-github")({
    clientID: process.env.GITHUB_CLIENT_ID,
    clientSecret: process.env.GITHUB_CLIENT_SECRET,
    baseURL: process.env.GITHUB_CLIENT_BASEURL || "http://localhost:1880/",
    users: JSON.parse(process.env.GITHUB_CLIENT_ROLES),
  });
}

// -- Protect the "HTTP IN" with Basic Auth
if (process.env.API_BASIC_AUTH_ROLES) {
  settings.httpNodeAuth = JSON.parse(process.env.API_BASIC_AUTH_ROLES);
}

if (process.env.NODE_RED_CREDENTIAL_SECRET) {
  settings.credentialSecret = process.env.NODE_RED_CREDENTIAL_SECRET;
}

module.exports = settings;
